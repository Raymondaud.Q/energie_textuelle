import re
class Result:
	def __init__(self, queryToken, distanceType, clusterType, template_path, template_line_number, content):
		self.distance = distanceType
		self.cluster = clusterType
		self.query = queryToken
		self.template_path = template_path
		self.template_line_number = template_line_number
		self.content = content

	def toString(self):
		return(self.query + " " + self.distance + " " + self.cluster + " " + str(self.template_line_number) + " :\n\t\t" + self.content)

	
	def buildResultList(path="Results/eval_results.txt"):
		f = open(path,"r", encoding="utf8")
		lines = f.readlines()
		lines = " ".join(lines)
		f.close()
		resultList = []
		experiences = re.split("</EXP>",lines)
		experiences = [ elem for elem in experiences if elem.strip() ]
		for exp in experiences :
			exp = re.split("\n",exp)
			exp = [ elem for elem in exp if elem.strip() ]
			info = exp[-1].split(" ")
			info = [ elem for elem in info if elem.strip() ]
			cnt = 0
			for line in [ elem for elem in exp[1:-1] if elem.strip() ]:
				res = Result(info[1], info[2], info[3], info[4], cnt, line)
				resultList.append(res)
				cnt+=1
		return resultList