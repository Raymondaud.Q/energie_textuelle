
# Résultats BASELINE : 

**Sur une toute petite portion du corpus dans le modèle de langage**

# 2-BASELINE

## **5 mots** : Résultat baseline **10 phrases** de **4**  **2-gram**  :

```
(enertex) sona@sona-HP-EliteBook-820-G2:~/Bureau/energie_textuelle (main)
└─▶ $:  python3 LanguageModelReader.py 2 4 10

Perse ou d’une voix juvénile.
Ni la police le dîner.
On fait de parler jamais.
Flaubert, cette petite comme endormie.
Glouère et pur et clair.
Protée qui ne désire encore.
Sénèque l’eut mis ; asseyez-vous.
Vers le soldat, comme toi.
Avoir toujours ce moment ?
Auprès d’elle, dans une soirée.
```

## **10 mots** : Résultat baseline **10 phrases** de **9**  **2-gram**   :

```
(enertex) sona@sona-HP-EliteBook-820-G2:~/Bureau/energie_textuelle (main)
└─▶ $:  python3 LanguageModelReader.py 2 9 10

Capistrau prit l’enfant un coup Valentine, quand j’étais une table.
Bâtiment des habitants afin qu’on la paume les diamants noirs.
Tadj dans mon honneur suivant le Capitaine pour répondre Benigno.
Vératine, qui lui murmurait avec mon nouveau à les philosophes.
Raffinement dont les bras avait autrefois pour celui près secs.
Livingstone encore pour ainsi sans confier le spectacle des interlocuteurs.
Sganarelle le trouble qu’elle méprisait, tandis qu’on ne savaient pas.
Comparé à gauche, il voulut boire, tout de métal terni.
Rogers, dit Timopht se précipitent, et qu’aucun degré du bateau.
Noir, tandis qu’une femme versait moitié démoli et là !
```
## **15 mots** : Résultat baseline **10 phrases** de **14**  **2-gram**   :

```
(enertex) sona@sona-HP-EliteBook-820-G2:~/Bureau/energie_textuelle (main)
└─▶ $:  python3 LanguageModelReader.py 2 14 10

Amenez-le se pencha au-dessus de lui serai mort depuis le euve, sûrs pour être crevassée.
Marseille, n’importe quel mortel n’avait garde ! je te ferais à court à vos menaces.
Huelva, 28 mai était coupé mes tristesses ensemble ; et les parents ce qui commandent.
Visiblement il dit oui, il pouvait prétendre que de Rogis qui étaient si sévère portique.
Henry, puis vous qu’un étranger, l’autre ses yeux pleins de cette soumission Pouvez-vous en fut.
Étouffée par le bout de feuilles prennent le cur débordait malgré l’expérience ne pas naturel.
Bien, je reviendrai et à mon mari le Capitaine et son père, qui serait impossible.
Ayez pitié, il essayait de bonheur Involontairement, Golder tressaillit, et à qui l’avait même temps.
Arrivées au regard en main à personne, quelle heure ou quatre mains, il arriva bientôt.
Présanges, hors d’haleine, elle aurait donc cette voix claire rose-thé qui habite l’appartement de bâton.
```



# 3-BASELINE

## **5 mots** : Résultat baseline **10 phrases** de **2**  **3-gram**   :

```(enertex) sona@sona-HP-EliteBook-820-G2:~/Bureau/energie_textuelle (main)
└─▶ $:  python3 LanguageModelReader.py 3 2 10

Borracho, président d’une petite ville.
Déjà, en partie à ce pays.
Chabanais, avec des belles compagnies.
Indien qui avait troublé l’imagination.
Pupin et cependant n’eut pas ses baisers.
Zina s’arrêta, fronça les sourcils.
Fischl, pourquoi ? interrompit Zakharief.
Esslenko, pour être aimée de lui.
Panama y a leurs innocents entretiens.
Reconnais-tu le pourpoint de son élève.
```

## **10 mots** : Résultat baseline **10 phrases** de **4**  **3-gram**  :

```
(enertex) sona@sona-HP-EliteBook-820-G2:~/Bureau/energie_textuelle (main)
└─▶ $:  python3 LanguageModelReader.py 3 4 10

Carmen Gonzalès, que pour faire au moins la vérité.
Model Club, ainsi le souvenir à se montrait de la pendule.
Thessalie à qui y avaient servi une illusion parfaite.
Pérou, par exemple, un Cudard, qui fait aux Indes.
Amis, voulut dire excellent cur ; ses doigts sur moi.
Dis-moi ce que s’il eût obsédée de son cheval.
Musée royal ? lui dit le temps de sa femme.
Retournons , dit Ben, mon petit Mon cher Elzéar.
Juan, soit plus en plus pour moi et pousse.
Soit, je vais te sentir dans ses fils à son intention.
```

## **15 mots** : Résultat baseline **10 phrases** de **6**  **3-gram**   :

```
(enertex) sona@sona-HP-EliteBook-820-G2:~/Bureau/energie_textuelle (main)
└─▶ $:  python3 LanguageModelReader.py 3 7 10

Temple, au péristyle immense, Et Benigno reparut au bout d’un instant d’affolement qu’elle était tuberculeuse.
Benigno, comme beaucoup aux poètes connus de moi, que je ne sais ce que je jouisse.
Inspruck, qui n’était plus, et l’esclave après leur mariage, ce qui fondit en la regardant.
Voyant que les révélant malgré nos prières, disant qu’on dit en elle une vieille femme.
Cache-toi, cache-toi pour les pauvres, dit Justine, étonnée, ne pût avoir le visage gardait d’en parler.
Isis n’a pas encore mort, pour ne pas lui ai fait de la circonstance, un corps nouveau.
Cloche, célébré par de violents éclats de vous avait toujours plus en plus femme, peut-on dire la vérité.
Serez-vous content ? dit-elle en outre avec son mari juif doit donc jouer son rôle.
Junior ne connaissait Mélie, qui me fait enfermer, je vous tiens, bandits ! dit Zénaïde.
Ethel en moins à la ferme suisse, et je vous demande pardon, et de ses petites faiblesses.
```