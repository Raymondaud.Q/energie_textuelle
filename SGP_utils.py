# ==========================
# SGP_utils.py
# ==========================
# This files contains a class useful for SGP related treatments
# ==========================


import re
from scipy.spatial import distance
from numpy import vstack
import numpy as np

# =================================================================================================
# SGP Utils Class
# =================================================================================================
class SGP:
    # regex used to parse labels
    # \u00C0-\u00FF means all the accents
    SGP_regex = r'\*[A-Z0-9]{7}\/[a-zA-Z\u00C0-\u00FF]*\/[a-zA-Z\u00C0-\u00FF]*'
    SENTENCE_regex = r'\*[a-zA-Z\u00C0-\u00FF]*\/[a-zA-Z\u00C0-\u00FF]*\/[a-zA-Z\u00C0-\u00FF]*'
    replacements = {
        "le": "l'",
        "la": "l'",
        "que": "qu'",
        "qui": "qu'",
        "du": "d'",
        "de": "d'",
        "me": "m'",
        "te": "t'",
        "ta": "t'",
        "se": "s'",
        "Le": "L'",
        "La": "L'",
        "Que": "Qu'",
        "Qui": "Qu'",
        "Du": "D'",
        "De": "D'",
        "Me": "M'",
        "Te": "T'",
        "Ta": "T'",
        "Se": "S'"}
    vowels = ["a", "e", "i", "o", "u", "y"]

    #Provides SGP labels and their index in template file 
    def template_labels_index(template_path="ResourcesN/template_eval"):
        label_index=[]
        with open(template_path) as f:
            lines = f.readlines()
            for line in lines:
                tokens=line.split(" ")
                li=[(tokens[i],i) for i in range(len(tokens)) if tokens[i][0] == '*' ]
                label_index.append(li)
        f.close()
        return label_index

    # Parse the sentence and get all labels with their associated forbidden words
    def parse_labels(sentence):
        labels = []
        farest_words = []
        matches = re.findall(SGP.SGP_regex, sentence)
        for match in matches:
            match_split = match.replace('*', '').split('/')
            labels.append(match_split[0])
            farest_words.append(match_split[1:])        
        return labels, farest_words

    # After POS labels are replaced, cleans up the output "*word/forbidden word/forbidden word" into simply => "word"
    def parse_sentence(sentence):
        matches = re.findall(SGP.SENTENCE_regex, sentence)
        for i in range(len(matches)):
            match_split = matches[i].replace('*', '').split('/')
            sentence = sentence.replace(matches[i], match_split[0],1)
        return sentence

    # Changes "le" into "l'" etc. when needed
    # /!\ unused
    def clean_quirks(output):
        for i in range(0, len(output)):
            tokenized = output[i].split(" ")
            for j in range(0, len(tokenized)):
                # Si on ne sort pas de la liste de tokens, que le token actuel est bien connu, et que la première lettre du prochain token est une voyelle
                if (0 <= j+1 < len(tokenized)) and (tokenized[j] in SGP.replacements.keys()) and (tokenized[j+1][0] in SGP.vowels):
                    tokenized[j] = SGP.replacements[tokenized[j]]
            output[i] = " ".join(tokenized)
        return output

    # Facade function to one of https://docs.scipy.org/doc/scipy/reference/spatial.distance.html
    def do_distance(a,b,dist_name="euclidean", minkowski_scalar=1, weights=None):
        if ( dist_name == "euclidean"):
            return distance.euclidean(a, b)
        elif ( dist_name == "seuclidean"):
            V = np.var(np.vstack((a,b)),axis=0)
            if ( np.isnan(np.sum(V)) or np.sum(V) == 0):
                V = np.ones(len(a))
            return distance.seuclidean(a,b,V)
        elif ( dist_name == "sqeuclidean"):
            return distance.sqeuclidean(a,b,weights)
        elif ( dist_name == "hamming"):
            return distance.hamming(a,b,weights)
        elif ( dist_name == "minkowski"):
            return distance.minkowski(a,b,minkowski_scalar,weights)
        elif ( dist_name == "mahalanobis"):
            V = np.cov(np.vstack((a,b)).T)
            if ( np.isnan(np.sum(V)) or np.sum(V) == 0 ):
                V = np.ones((len(a),len(b)))
            return distance.mahalanobis(a,b,V)
        elif ( dist_name == "braycurtis"):
            return distance.braycurtis(a,b,weights)
        elif ( dist_name == "canberra"):
            return distance.canberra(a,b,weights)
        elif ( dist_name == "chebyshev"):
            return distance.chebyshev(a,b,weights)
        elif ( dist_name == "cityblock"):
            return distance.cityblock(a,b,weights)
        elif ( dist_name == "correlation"):
            return distance.correlation(a,b,weights)
        elif ( dist_name == "cosine"):
            return distance.cosine(a,b,weights)

# =================================================================================================