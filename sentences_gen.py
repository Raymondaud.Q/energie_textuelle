# -*- coding: utf-8 -*-

# ==========================
# sentences_gen.py
# ==========================
# This file is in charge of doing the actual sentence generation
# Because it's using serialized files, please ensure that parse_and_serialized did his process beforehand
# ==========================
# USAGE : python3 sentences_gen.py QUERY <Optional distName> <Optional clusteringName> <Optional templatePath>
# ==========================

import sys, time, pickle
import numpy as np
from SGP_utils import SGP


# =================================================================================================
# Parsing argument & inits
# =================================================================================================
argv = sys.argv; argc = len(argv) 


# the query is passed as an argument when executing the python script*
# it has to be a mono-token

DISTANCE_NAME= argv[2] if ( argc > 2 ) else "minkowski"
CLUSTERING_METHOD = argv[3] if ( argc > 3 ) else "none"
SGP_PATH = argv[4] if ( argc > 4 ) else "./ResourcesN/template_eval"
WORD2VEC_SERIALIZED_PATH = "./MODELS/embeddings_serialized.p"
ASSOCIATIVE_TABLE_SERIALIZED_PATH = "./MODELS/associative_table_serialized.p"
TERM_FREQ_SERIALIZED_PATH = "./MODELS/words_freq.p"
WORD2VEC_KMEANS_CLUSTERING =  "./MODELS/w2v_kclustering.p"
WORD2VEC_NN_CLUSTERING =  "./MODELS/w2v_nclustering.p"

if argc < 2 : 
    print("ERROR Missing QUERY argument.\n Example usage : \n"+
        "-\tpython3 sentences_gen.py QUERY <Optional distName> <Optional clusteringName> <Optional templatePath>\n"+
        "// ! \\\\   - Default distName = "+DISTANCE_NAME+"\n"+
        "// ! \\\\   - Default clusteringName = "+CLUSTERING_METHOD+"\n"+
        "// ! \\\\   - Default templatePath = "+SGP_PATH+"\n"+
        "// ! \\\\   - Make sure that you have already run parse_and_serialize.py ! \n"+
        "-\tpython3 sentences_gen.py tristesse minkowski knn\n"+
        "-\tpython3 sentences_gen.py tristesse euclidean none\n"+
        "-\tpython3 sentences_gen.py tristesse cityblock kmeans\n"+
        "-\tpython3 sentences_gen.py tristesse")
    exit()
else : 
    QUERY = argv[1]

# =================================================================================================
# Loading serialized files
# =================================================================================================
with open(WORD2VEC_SERIALIZED_PATH, 'rb') as file:
    embeddings = pickle.load(file)

with open(ASSOCIATIVE_TABLE_SERIALIZED_PATH, 'rb') as file:
    associative_table = pickle.load(file)

with open(SGP_PATH, 'r', encoding="utf8") as file:
    all_SGP_strings = file.readlines()
    for sentence in all_SGP_strings:
        sentence = sentence.replace("_", " ")
with open(TERM_FREQ_SERIALIZED_PATH, 'rb') as file:
    term_freq_dic = pickle.load(file)
nb_occ=sum(term_freq_dic.values())
if ( CLUSTERING_METHOD == "kmeans"):
    with open(WORD2VEC_KMEANS_CLUSTERING, 'rb') as file:
        fitted_kmeans = pickle.load(file)

elif ( CLUSTERING_METHOD == "knn"):
    with open(WORD2VEC_NN_CLUSTERING, 'rb') as file:
        fitted_nn = pickle.load(file)

# =================================================================================================
# Pick up candidates relative to a QUERY and fitting a label according to clusterization options
# =================================================================================================
def getCandidates(label,query_embedding):
    if (CLUSTERING_METHOD == "kmeans"):
        if ( label in fitted_kmeans ):
            cluster_num = fitted_kmeans[label].predict(query_embedding.reshape(1, -1))[0]
            embeddings_val =[value for value in embeddings.values()]
            indexes = np.where(fitted_kmeans[label].labels_ == cluster_num)
            val_embeddings = [ val for val in embeddings.keys()]
            clustered_words = []
            for index_list in indexes:
                for index in index_list:
                    clustered_words.append(val_embeddings[index])
            return [x for x in associative_table[label] if ( x in clustered_words or len(clustered_words) < 1)] 
    elif (CLUSTERING_METHOD == "knn"):
        if ( label in fitted_nn ):
            clustered_index = fitted_nn[label].kneighbors(query_embedding.reshape(1, -1), 100, return_distance=False)[0]
            clustered_words = [ associative_table[label][i] for i in clustered_index  ]
            return  [x for x in associative_table[label] if ( x in clustered_words or len(clustered_words) < 1)] 
    return [x for x in associative_table[label]]


def main():
    all_SGP =[]
    for sentence in all_SGP_strings:
        labels, farest_words = SGP.parse_labels(sentence)
        all_SGP.append({"sentence": sentence, 
                        "labels": labels,
                        "farest_words": farest_words,
                        "forbidden_words": []})
    all_sentence = []
    try:
        query_embedding = np.asarray(embeddings[QUERY]) # fetch the embedding of the query
    except KeyError:
        print("The word " + QUERY +" is unknown in the database.")
        return
    for sgp in all_SGP:
        cnt = 0
        for label in sgp['labels']:
            closest_word = 'none-token'
            candidates_words = getCandidates(label,query_embedding) # fetch the candidates words in the associative table
            dists = {}
            for candidate in candidates_words : # evaluate the euclidian distance between the label and each candidates
                if candidate not in sgp['forbidden_words'] and candidate in term_freq_dic : # he has to not be a forbidden words
                    distance_far = 0
                    try:
                        candidate_embedding = np.asarray(embeddings[candidate]) # he also has to exist
                    except KeyError:
                        continue # skip the candidate that doesn't exist in our database
                    distance_qc = SGP.do_distance(query_embedding, candidate_embedding, dist_name=DISTANCE_NAME) - ( term_freq_dic[candidate] / nb_occ )
                    for farest in sgp['farest_words'][cnt] :
                        distance_far += SGP.do_distance(embeddings[farest],candidate_embedding, dist_name=DISTANCE_NAME)
                    dists[candidate] = (distance_far/2,distance_qc,candidate)
            best_word_score = None
            for dist_candidate in dists.values() :
                if ( best_word_score == None ) or ( dist_candidate[0]-dist_candidate[1] > best_word_score ) :
                    closest_word = dist_candidate[2]
                    best_word_score = dist_candidate[0]-dist_candidate[1]
            sgp['forbidden_words'].append(closest_word)
            sgp['sentence'] = sgp['sentence'].replace(label, closest_word,1)
        all_sentence.append(sgp['sentence'])
        cnt+=1
    for s in all_sentence:
        print(SGP.parse_sentence(s))

if __name__ == "__main__":
    print("<EXP> sentence_gen.py "+QUERY+" "+DISTANCE_NAME+" "+CLUSTERING_METHOD+" "+SGP_PATH)
    start = time.perf_counter()
    main()
    end = time.perf_counter()
    print("<TIME>" + str(end-start) + "sec </TIME> </EXP>\n")