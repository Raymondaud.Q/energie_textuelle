# -*- coding: utf-8 -*-

# ==========================
# LanguageModelFactory.py
# ======== ARGS ============
# $1 = N-GRAM SIZE
# $2 = DATA_PATH
# $3 = RESULT_PATH
# ==========================
# Walks recursively the folder $2 in order to generate a $1-gram Language Model in the $3 folder
# ==========================
# FIX - New version faster (useless disk R/W removal)

import os, re, sys, psutil
from nltk.util import ngrams

# ==========================
# Parsing argument & inits
# ==========================
# N-GRAM SIZE : BIGRAMS BY DEFAULT
N_GRAM_SIZE = 2 if ( len(sys.argv) < 2 or not sys.argv[1] ) else int(sys.argv[1])
# DATA_PATH : PATH TO TEXT FILE FOLDERS ON WICH YOU WANT TO ESTABLISH THE LANGUAGE MODEL : "DATA/" BY DEFAULT
DATA_PATH='DATA/' if (len(sys.argv) < 3 or not sys.argv[2] ) else sys.argv[2]
# RESULT_PATH : PATH TO FOLDER WHERE WE SAVE THE SERIALIZED LANGUAGE MODEL ; "MODELS/" BY DEFAULT
RESULT_PATH='MODELS/' if ( len(sys.argv) < 4 or not sys.argv[3] ) else sys.argv[3]
RAM_LIMIT = 50 if ( len(sys.argv) < 5 or not sys.argv[4] ) else int(sys.argv[4])
# N-GRAM OCCURENCES DICTIONARY
n_gram_dict={}
#EXCLUDED TOKENS
exclude=['CHAPITRE', 'Chapitre', 'XI', 'XV', 'VII' , 'VIII', 'XC', 'II','III', 'XCLV' ]


# ============ ARGS ======================
# s = String : A sentence
# n = Integer : N in N-Gram 
# ========================================
# Generates a list of n-grams from a sentence
# ============ RETURNS ===================
# List of String : Returns a list of n-grams 
def generate_ngrams(s, n):
    s = re.sub(' ', ' ', s)
    s = re.sub('ﬀ','ff',s)
    tokens = [re.sub("[^A-zÀ-ÿ0-9\"\'\`\’\,\`\-\;\.\?\!]",' ',token) for token in s.split(" ") if token != ""]
    tokens = list(filter(None, tokens))
    tokens = [ele for ele in tokens if ele.strip()]
    return list(ngrams(tokens, n))

# ============ ARGS ======================
# num_gram = Integer : N in N-grams
# n_gram_dict : Dictonnary : The Language Model to fill
# content : Content of a text file as list of line
# ========================================
# Add n-gram in the dictionnary and returns it
# ============ RETURNS ===================
# Dictionnary : Language Model KEY = N-gram, VALUE = frequency
def buildDict(num_gram, n_gram_dict, content):
    for line in content:
        line.encode() # utf-8 by default
        toPass = False
        # If any token in the n-gram is exclude then pass
        for elem in exclude :
            if ( elem in line or elem in ' '):
                toPass = True
                break
        # Else fills up the dictionnary by parsing each line and counting n-grams
        if not toPass :
            line = line.replace(" - ", "-")
            ngrams = generate_ngrams(line, num_gram)
            for ngram in ngrams :
                tokens = [ele for ele in ngram if ele.strip()]
                if not str(ngram) in n_gram_dict :
                    n_gram_dict[str(ngram)]=1
                else :
                    n_gram_dict[str(ngram)]+=1
    return n_gram_dict

# ============ ARGS ======================
# path = String : data folder path
# num_gram : Integer : N in N-grams
# n_gram_dict : Dictonnary : The Language Model to fill
# ========================================
# Fills up the N-grams dictionnary with the content of a file
# Writes it on the disk
# ============ RETURNS ===================
# Dictionnary : Language Model KEY = N-gram, VALUE = frequency
def langModelFromFile(path, num_gram, n_gram_dict):
    f = open(path,'r', encoding="utf8")

    n_gram_dict = buildDict(num_gram, n_gram_dict, f.readlines())

    return n_gram_dict

# ============ ARGS ======================
# DATA_PATH = String : data folder path
# num_gram : Integer : N in N-grams
# n_gram_dict : Dictonnary : The Language Model to fill
# ========================================
# Build a Language Model by walking a textfiles data folder recursively 
# ============ RETURNS ===================
# None : The language model is written on the disk in subfunctions
def buildModelFromDir(DATA_PATH, num_gram, n_gram_dict):
    for root, dirs, files in os.walk(DATA_PATH):
        cnt = 0
        for name in files:

            if name.endswith((".seg")):
                print(str(cnt)+"/"+str(len(files)) + " Adding "+name+" to langModel\n")
                print("LangModel Size : " + str(len(n_gram_dict)))
                n_gram_dict = langModelFromFile( os.path.join(root,name), num_gram, n_gram_dict)
                cnt+=1

            if psutil.virtual_memory().percent > RAM_LIMIT :
                print('¯\\_(ツ)_/¯ Language model partially built, less than '+str(100-RAM_LIMIT)+'% RAM available - ¯\\_(ツ)_/¯\n -- checkout the config line 26')
                model = open(RESULT_PATH+str(num_gram)+"-gram.txt",'w')
                for key in n_gram_dict:
                    strKeys = key.split(', ')
                    keywords = "\t".join([ke for ke in strKeys])
                    keywords = re.sub('[\'(*)]*', "", keywords ) + "\t"
                    keywords = keywords.replace(" ","")
                    keywords = keywords.replace("\t\t","\t")
                    model.write(keywords + str(n_gram_dict[key]) + "\n")
                model.close()
                exit()

    print('Language model built successfully !')
    model = open(RESULT_PATH+str(num_gram)+"-gram.txt",'w')
    for key in n_gram_dict:
        strKeys = key.split(', ')
        keywords = "\t".join([ke for ke in strKeys])
        keywords = re.sub('[\'(*)]*', "", keywords ) + "\t"
        keywords = keywords.replace(" ","")
        keywords = keywords.replace("\t\t","\t")
        model.write(keywords + str(n_gram_dict[key]) + "\n")
    model.close()

if __name__ == "__main__":

    buildModelFromDir(DATA_PATH,N_GRAM_SIZE,n_gram_dict)