
. enertex/bin/activate
for query in tristesse amour joie haine bleu 
do
	for cluster in knn kmeans none
	do
		for dist in  chebyshev cityblock braycurtis canberra cosine correlation minkowski sqeuclidean seuclidean euclidean mahalanobis
		do
			python3 sentences_gen.py $query $dist $cluster 
		done
	done
done
deactivate