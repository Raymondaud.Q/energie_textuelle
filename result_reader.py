import sys
from result import Result 
from SGP_utils import SGP

template_num_OPT=None
query_OPT=None
cluster_OPT=None
distance_OPT=None
RESULT_PATH="Results/eval_results.txt"

args = sys.argv
for i in range(1,len(args)):
    for arg in args[1:]:
        if ( arg in ["tristesse", "amour", "joie", "haine", "bleu"] ):
            query_OPT = arg
        elif ( arg in ["knn", "kmeans", "none"] ):
            cluster_OPT = arg
        elif ( arg in ["chebyshev", "cityblock", "braycurtis", "canberra", "cosine", "correlation", "minkowski", "sqeuclidean", "seuclidean", "euclidean", "mahalanobis"] ):
            distance_OPT = arg
        elif ( arg.isnumeric() ):
            template_num_OPT=arg
        else:
            RESULT_PATH=arg

# List of Result from eval_script's output
results = Result.buildResultList(path=RESULT_PATH)
# List of List of tuple (POS, POS index in the current line )
label_index_list = SGP.template_labels_index(template_path=results[0].template_path)

for res in results:
    if ( (template_num_OPT == None or template_num_OPT == str(res.template_line_number)) and 
    ( query_OPT == None or query_OPT == res.query ) and 
    ( cluster_OPT == None or cluster_OPT == res.cluster) and 
    ( distance_OPT == None or distance_OPT == res.distance ) ):
        print('───────-----───────------───────---')
        print(" Template line number  " + str(res.template_line_number)) 
        print(" POS-TAG Index :" ) 
        for label in label_index_list[res.template_line_number]:
        	print("\t"+str(label))
        print(" Query:" + res.query + "\n Clustering:" + res.cluster + "\n Distance:" + res.distance + "\n Result ───────▶" + res.content)
print('───────-----───────------───────---')
print("From Result file : " + RESULT_PATH  )