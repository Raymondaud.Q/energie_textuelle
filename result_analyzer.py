import sys
from result import Result 
from SGP_utils import SGP
import seaborn as sns
import matplotlib.pyplot as plt

template_num_OPT=None
query_OPT=None
cluster_OPT=None
distance_OPT=None
RESULT_PATH="Results/eval_results.txt"

args = sys.argv
for i in range(1,len(args)):
    for arg in args[1:]:
        if ( arg in ["tristesse", "amour", "joie", "haine", "bleu"] ):
            query_OPT = arg
        elif ( arg in ["knn", "kmeans", "none"] ):
            cluster_OPT = arg
        elif ( arg in ["chebyshev", "cityblock", "braycurtis", "canberra", "cosine", "correlation", "minkowski", "sqeuclidean", "seuclidean", "euclidean", "mahalanobis"] ):
            distance_OPT = arg
        elif ( arg.isnumeric() ):
            template_num_OPT=arg
        else:
            RESULT_PATH=arg

# List of Result from eval_script's output
results = Result.buildResultList(path=RESULT_PATH)
# List of List of tuple (POS, POS index in the current line )
label_index_list = SGP.template_labels_index(template_path=results[0].template_path)


hist = {}

for res in results:
    if ( (template_num_OPT == None or template_num_OPT == str(res.template_line_number)) and 
    ( query_OPT == None or query_OPT == res.query ) and 
    ( cluster_OPT == None or cluster_OPT == res.cluster) and 
    ( distance_OPT == None or distance_OPT == res.distance ) ):
        resplit = res.content.split(" ")
        resplit = [ele for ele in resplit if ele.strip()]
        for posIndex in [ label[-1] for label in label_index_list[res.template_line_number] ]:
            word = resplit[posIndex]
            if word in hist:
                hist[word] = hist[word]+1
            else :
                hist[word] = 1

import pandas as pd
df = pd.DataFrame(list(hist.items()), columns=["Word","Occurence"] )
print("Histogram=",hist)
sns.set_theme(style="whitegrid")
ax = sns.barplot(x="Word", y="Occurence", data=df)
plt.gcf().set_size_inches(15,15)
plt.xticks(rotation=90, fontsize=10)
filename = str(template_num_OPT)+"_"+str(query_OPT)+"_"+str(cluster_OPT)+"_"+str(distance_OPT)+"_"+str(RESULT_PATH.split('.')[0].split('/')[-1])+".png"
plt.title(filename)
plt.savefig("Results/charts/"+filename,  bbox_inches = 'tight')
print("\n/!\\ Barplot saved in "+filename)

