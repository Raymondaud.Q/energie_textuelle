# -*- coding: utf-8 -*-

# ==========================
# LanguageModelReader.py
# ======== ARGS ============
# $1 = N-GRAM SIZE
# $2 = N-GRAM NUMBER
# $3 = NUM_SENTENCE
# $4 = MODEL_FOLDER_PATH
# ==========================
# Loads a language model from $4 and generates $3 sentences formed by $2 $1-grams 
# ==========================

import os, re, sys, random, pickle, psutil

fin_gen = ['!', '.', '?']
quiet=True

# =======================================
# Parsing argument & inits
# =======================================
# N-GRAM SIZE : BIGRAMS BY DEFAULT
N_GRAM_SIZE = 2 if ( len(sys.argv) < 2 or not sys.argv[1] ) else int(sys.argv[1])
# N-GRAM NUMBER : NUMBER OF NGRAMS USED IN ORDER TO GENERATE A SENTENCE
N_GRAM_NUMBER = 5 if ( len(sys.argv) < 3 or not sys.argv[2] ) else int(sys.argv[2])
# NUM_SENTENCE : NUMBER OF SENTENCE TO GENERATE
NUM_SENTENCE = 10  if ( len(sys.argv) < 4 or not sys.argv[3] ) else int(sys.argv[3])
# MODEL_FOLDER_PATH : PATH TO LANGUAGE MODEL FOLDER
MODEL_FOLDER_PATH = 'MODELS/' if ( len(sys.argv) < 5 or not sys.argv[4] ) else sys.argv[4]
MODEL_PATH= MODEL_FOLDER_PATH+str(N_GRAM_SIZE)+"-gram.txt"
RAM_LIMIT = 50 if ( len(sys.argv) < 6 or not sys.argv[5] ) else int(sys.argv[5])
# DICTONNARY KEY = FIRT WORD, VALUE = LIST OF N-GRAM WHICH BEGIN WITH THE KEY 
dict_fw_ngram_list = {}

# ============ ARGS ======================
# listToStr = List of String 
# ========================================
# Merge this list in a string
# ============ RETURNS ===================
# String : The merged list of string 
def customJoin(listToStr):
    if ( type(listToStr) is list):
        return " "+" ".join(listToStr)
    else :
        return listToStr 

# ============ ARGS ======================
# generated = String : sentence buffer
# dict_fw = Dictionnary : KEY = FIRT WORD, VALUE = LIST OF N-GRAM WHICH BEGIN WITH THE KEY
# depth = Integer : Number of n-grams to add to the sentence buffer
# cnt = Integer : Count recursions at the same depth in order to avoid infinite loops
# quiet = Boolean : Print log or not$
# ========================================
# Generates a sentences recursively by adding n-grams from a language model
# ============ RETURNS ====================
# String : A sentence 
def addNGram(generated, dict_fw, depth, cnt=0, quiet=quiet):
    # If we finished recursion then we return the result
    if ( depth == 0 or generated=='' or cnt > 10 ):
        return generated
    # We get the last token of the last n-gram added
    lstGen = generated.split(" ")
    lstGen = list(filter(None, lstGen))
    lastGen = lstGen[-1]

    if (lastGen in dict_fw ):
        
        if ( depth == 1):
            # If the last n-gram then his last token must contain a fin_gen
            fitting_ngrams = [ elems for elems in dict_fw[lastGen] for fin in fin_gen if ( fin in elems[-2] and len(elems[-2]) > 2 )]
        elif ( depth > 1 ): 
            fitting_ngrams = [ elems for elems in dict_fw[lastGen] if( not fin_gen in elems) ]   
        weight_list = [ int(i[-1]) for i in fitting_ngrams ] # Récupération des poids

        # If it's a dead end then we turn back
        if ( len(fitting_ngrams) == 0 or len(weight_list) == 0):
            if ( not quiet ) : print("Rollback at Step "+ str(depth) + " => " + generated)
            cnt+=1
            generated = generated.replace(' '+lastGen, '').replace(lastGen, '')
            if ( generated == '' or cnt > 10):
                return
            return addNGram(generated, dict_fw, depth+1, cnt=cnt )

        # We add a token and continue recursively
        add = random.choices(fitting_ngrams, weights=weight_list, k=1)[0][1:-1]
        generated += customJoin(add)
        if ( not quiet ) : print("Step "+ str(depth) + " => " + generated)
        return addNGram( generated, dict_fw, depth-1, cnt=cnt )


    # If it's a dead end then we turn back
    if ( not quiet ) : print("Stuck at Step "+ str(depth) + " => " + generated)
    cnt+=1
    generated = generated.replace(' '+lastGen, '').replace(lastGen, '')
    if ( generated == ''  or cnt > 10):
        return
    return addNGram(generated, dict_fw, depth+1, cnt=cnt )

# ============ ARGS ======================
# NUM_SENTENCE = Integer : number of sentence to generate 
# dict_fw = Dictonnary : KEY = FIRT WORD, VALUE = LIST OF N-GRAM WHICH BEGIN WITH THE KEY
# ========================================
# Generates a given number of sentences
# ============ RETURNS ===================
# List of String : Generated sentences
def generateSentence(NUM_SENTENCE, dict_fw):
    sentences = []
    while ( len(sentences) < NUM_SENTENCE ) :
        start = random.choice(list(dict_fw.keys()))
        words = start.split()
        while ( len(words[0]) <= 1 or  
                not start in dict_fw or 
                not ( words[0][0].isupper() and words[0][1].islower() )):
            start = random.choice(list(dict_fw.keys()))
            words = start.split()
        sentences.append(addNGram(start, dict_fw, N_GRAM_NUMBER))
        sentences = list(filter(None, sentences))
    return sentences


if __name__ == "__main__":

    if ( os.path.exists( MODEL_FOLDER_PATH + str(N_GRAM_SIZE)+"-gram_serialized.p" ) ):
        with open( MODEL_FOLDER_PATH + str(N_GRAM_SIZE)+"-gram_serialized.p", 'rb' ) as file:
            dict_fw_ngram_list = pickle.load(file)
        file.close()
    else :   
        with open(MODEL_PATH,'r', encoding="utf8") as f:
            cnt = 0
            for line in f :
                term = f.readline().replace('\n','').split("\t")
                # Get First and Last word of the ngram
                fw = term[0]
                if ( fw in dict_fw_ngram_list ):
                    dict_fw_ngram_list[fw].append(term)
                else :
                    dict_fw_ngram_list[fw]=[term]
                cnt+=1
                if psutil.virtual_memory().percent > RAM_LIMIT:
                    print('¯\\_(ツ)_/¯ Serialized Language model partially built, less than '+str(100-RAM_LIMIT)+'% RAM available - ¯\\_(ツ)_/¯\n-- checkout the config line 31')
                    break
            f.close()

        with open( MODEL_FOLDER_PATH+str(N_GRAM_SIZE)+"-gram_serialized.p", 'wb' ) as file:
            pickle.dump(dict_fw_ngram_list, file)
        file.close()

        
    for sentence in generateSentence(NUM_SENTENCE, dict_fw_ngram_list):
        print(sentence)
    
