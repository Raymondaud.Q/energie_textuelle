# -*- coding: utf-8 -*-

# ==========================
# parse_and_serialize.py
# ==========================
# This file is in charge of parsing and serializing every resources given in order to do sentences generation (like embeddings)
# Tasks to do here are divided in stages for memory management purposes
# Results are serialized at the end of each stage, so that it can be loaded in memory for other files later on, not having to re-do everything
# ==========================

import pickle
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cluster import MiniBatchKMeans
from sklearn.neighbors import NearestNeighbors
import os
import numpy as np
import sys

stage = int(sys.argv[1]) if ( len(sys.argv) > 1 ) else 0 
WORD2VEC_EMBEDDINGS_PATH = sys.argv[2] if ( len(sys.argv) > 2 ) else "./ResourcesN/embeddings-Fr/embeddings-Fr.txt" 
ASSOCIATIVE_TABLE_PATH = sys.argv[3] if ( len(sys.argv) > 3 ) else "./ResourcesN/TableAssociative/TableAssociative"
TERM_FREQ_PATH = sys.argv[4] if ( len(sys.argv) > 4 ) else "./MODELS/words_freq.p"
WORD2VEC_KMEANS_CLUSTERING = sys.argv[5] if ( len(sys.argv) > 5 ) else "./MODELS/w2v_kclustering.p"
WORD2VEC_NN_CLUSTERING = sys.argv[6] if ( len(sys.argv) > 6 ) else "./MODELS/w2v_nclustering.p"
WORD2VEC_SERIALIZED_PATH = "./MODELS/embeddings_serialized.p"
VEC2WORD_SERIALIZED_PATH = "./MODELS/reverse_embeddings_serialized.p"
MEGALITE_PATH = "./DATA/"

if ( stage == 1 or stage == 0 ):
    print('Parsing and serializing WORD2VEC Megalite Embeddings...')
    embeddings = {}
    reverse_embeddings = {}
    with open(WORD2VEC_EMBEDDINGS_PATH, "r", encoding="utf8") as file:
        # for each line of the file, parse and get all the floats
        for line in file:
            word, coefs = line.split("\t")
            numeric_const_pattern = r"""
            [-+]? # optional sign
            (?:
                (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
                |
                (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
            )
            # followed by optional exponent part if desired
            (?: [Ee] [+-]? \d+ ) ?
            """
            regfloat = re.compile(numeric_const_pattern, re.VERBOSE)
            coefs = regfloat.findall(coefs)
            em_values = [float(i) for i in coefs]
            embeddings[word] = em_values
            reverse_embeddings[str(em_values)] = word
    pickle.dump(embeddings, open(WORD2VEC_SERIALIZED_PATH, "wb"))
    pickle.dump(reverse_embeddings, open(VEC2WORD_SERIALIZED_PATH, "wb"))
    del embeddings
    del reverse_embeddings

if ( stage == 2 or stage == 0 ):
    associative_table = {}
    reversed_associative_table = {}
    print('Parsing and serializing Associative Table (Freeling) ...')
    with open(ASSOCIATIVE_TABLE_PATH, "r", encoding="utf8") as file:
        for line in file:
            pos, words = line.split(maxsplit=1)
            words = words.replace('\n', '')
            words = words.split("\t")
            associative_table[pos] = words
        for word in words :
            if ( not word in reversed_associative_table ):
                reversed_associative_table[word] = []
                reversed_associative_table[word].append(pos)
            else:
                if ( not pos in reversed_associative_table[word] ) :
                    reversed_associative_table[word].append(pos)
    pickle.dump(associative_table, open("./MODELS/associative_table_serialized.p", "wb"))
    pickle.dump(reversed_associative_table, open("./MODELS/reversed_associative_table_serialized.p", "wb"))
    del associative_table
    del reversed_associative_table
    del words


if ( stage == 3 or stage == 0 ):
    corpus = []
    print('Computing and serializing Term-Frequency dictionnary for the whole Dataset')
    # browsing all the dirs
    for root, dirs, files in os.walk(MEGALITE_PATH):
        for name in files:
            if name.endswith((".seg")):
                corpus.append(open(os.path.join(root,name), 'r', encoding="UTF-8").read()) # appending all the text in the corpus array
    vectorizer = CountVectorizer()
    X = vectorizer.fit_transform(corpus) # counting every frequency of the corpus
    X_array = X.toarray()
    names = vectorizer.get_feature_names_out() # we now have an array that gives the frequency for each word
    sums = X_array.sum(axis=0) # we sum all the columns, meaning we sum all the frequencies for each documents, meaning we get the frequency for the whole corpus
    words_freq = {}
    for i in range(0, len(names)):
        words_freq[names[i]] = sums[i]
    pickle.dump(words_freq, open(TERM_FREQ_PATH, "wb"))
    del corpus
    del words_freq
    del names
    del sums

if ( stage == 4 or stage == 0 ):
    kmeans = {}
    nn = {}
    print('Computing and serializing K-Means Clustering & KNN on each POS-TAG embeddings set')
    with open(WORD2VEC_SERIALIZED_PATH, 'rb') as file:
        embeddings = pickle.load(file)
    with open("./MODELS/associative_table_serialized.p", 'rb') as file:
        ta = pickle.load(file)
    # For each label in the associative table 
    for pos_tag in ta.keys():
        # We gather in an array the embeddings from each word linked to the label 
        pos_embeddings = [ embeddings[word] for word in ta[pos_tag] if ( word in embeddings.keys() )]
        # If the label contains more than ten words then we : 
        if ( len(pos_embeddings)>10 ):
            # Apply a K-Mean Clustering on these embeddings
            kmeans[pos_tag] = MiniBatchKMeans(
                n_clusters=5,
                batch_size=10,
                max_iter=10).fit(pos_embeddings)
            # Apply a Neirest Neighbors clustering on these embeddings
            nn[pos_tag] = NearestNeighbors(
                n_neighbors=500,
                radius=0.8,
                n_jobs=-1).fit(pos_embeddings)
        else :
            print("/ ! \\ Warning -- Cannot compute " + pos_tag + " K-Means/NN : only " + str(len(pos_embeddings)) + " samples")
    # Serialize fitted K-Means & Nearest Neighbors models
    pickle.dump(kmeans, open(WORD2VEC_KMEANS_CLUSTERING, "wb"))
    pickle.dump(nn, open(WORD2VEC_NN_CLUSTERING, "wb"))

print("Stage(s) done successfully !")