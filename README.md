
https://gitlab.com/Raymondaud.Q/energie_textuelle

# Enerlab Auto Setup

* chmod +x install.sh
* ./install.sh  

# Enerlab Manual Setup 

* python3 -m venv enerlab
* source enerlab/bin/activate
* pip install nltk scipy sklearn seaborn psutil
* mkdir ResourcesN
* unzip -d ResourcesN/ ./ResourcesN.zip
* python3 parse_and_serialize.py


# Activate Deactivate venv 

* Activation venv : `source enerlab/bin/activate`
* Désactivation venv : `deactivate`


# Run BASELINE Automated Sentence Generator:

**----/!\\----**  
**Optional parameters** : `num_gram`, `nb_ngram`, `nb_sentences` `<DATA_PATH>/` and `<MODEL_PATH>/` **set to `2`, `5`, `10`, `DATA/` and `MODELS/` by default**  
**You can put your raw textfiles data in DATA/ folder and run the following commands specifying nothing, only `num_gram` & `nb_ngram`, ...**  
**----/!\\----** 

* source enerlab/bin/activate
* **To generate a language model** of `num_gram`-grams with all .txt content of data dir `<DATA_PATH>/`* in `<MODEL_PATH>/`
	* python3 LanguageModelFactory.py `num_gram` `<DATA_PATH>/` `<MODEL_PATH>/` 
* **To generate `nb_sentences` sentences** by merging `nb_ngram` ngrams from a language model of `num_gram`-grams already built in `<MODEL_PATH>/` 
	* python3 LanguageModelReader.py `num_gram` `nb_ngram` `nb_sentences` `<MODEL_PATH>/`
* deactivate

[You can have a look on previous results here !](https://gitlab.com/Raymondaud.Q/energie_textuelle/-/blob/main/BASELINE_RESULTS.md)

# Run Word2Vec & Freeling Based Sentence Autocompletion/Generator 

* source enerlab/bin/activate
* python3 sentences_gen.py tristesse chebyshev none
* python3 sentences_gen.py tristesse cityblock none ResourcesN/templates_basiques
* python3 sentences_gen.py tristesse braycurtis
* python3 sentences_gen.py tristesse canberra none
* python3 sentences_gen.py tristesse cosine none
* python3 sentences_gen.py tristesse correlation none
* python3 sentences_gen.py tristesse
* python3 sentences_gen.py tristesse sqeuclidean none
* python3 sentences_gen.py tristesse seuclidean none
* python3 sentences_gen.py tristesse euclidean none
* python3 sentences_gen.py tristesse mahalanobis none

* python3 sentences_gen.py tristesse chebyshev knn
* python3 sentences_gen.py tristesse cityblock knn
* python3 sentences_gen.py tristesse braycurtis knn
* python3 sentences_gen.py tristesse canberra knn
* python3 sentences_gen.py tristesse cosine knn
* python3 sentences_gen.py tristesse correlation knn
* python3 sentences_gen.py tristesse minkowski knn
* python3 sentences_gen.py tristesse sqeuclidean knn
* python3 sentences_gen.py tristesse seuclidean knn
* python3 sentences_gen.py tristesse euclidean knn
* python3 sentences_gen.py tristesse mahalanobis knn

* python3 sentences_gen.py tristesse chebyshev kmeans
* python3 sentences_gen.py tristesse cityblock kmeans
* python3 sentences_gen.py tristesse braycurtis kmeans
* python3 sentences_gen.py tristesse canberra kmeans
* python3 sentences_gen.py tristesse cosine kmeans
* python3 sentences_gen.py tristesse correlation kmeans
* python3 sentences_gen.py tristesse minkowski kmeans
* python3 sentences_gen.py tristesse sqeuclidean kmeans
* python3 sentences_gen.py tristesse seuclidean kmeans
* python3 sentences_gen.py tristesse euclidean kmeans
* python3 sentences_gen.py tristesse mahalanobis kmeans
* deactivate

## Evaluation

Evaluation script (eval_script.sh) implements the protocol with the following queries : 
* tristesse 
* amour 
* joie 
* haine
* bleu 

With the eval template (ResourcesN/template_eval) on each distances :
* chebyshev
* cityblock
* braycurtis
* canberra
* cosine
* correlation
* minkowski
* sqeuclidean
* seuclidean
* euclidean
* mahalanobis 

And each embeddings (sub)-representation :
* knn
* kmeans
* none (default word2vec)


**Results are provided in eval_results.txt & paper_results.txt**
You can easily redirect output in a file by launching the following command :
**`./eval_script.sh > ./Results/eval_results.txt`**

## Results reader & filter script

You can lists and filter the results according to the **unordered and optional** args you pass to **`result_reader.py`**.  
Note that available args are the **values listed** above, **an integer corresponding to the template line number** and **a path to an eval_script's output textfile (./Results/eval_results.txt for example)** :

You can change the path to the txt result file line 14.

* `python3 result_reader.py 5` will show you all the results on 5th line on the template
* `python3 result_reader.py minkowski 5` will show you all the results on 5th line on the template with the minkowski distance
* `python3 result_reader.py euclidean knn 0` will show you all the results on 0th line on the template with the euclidean distance and the knn clustering
* `python3 result_reader.py cityblock tristesse 4 none` will show you all the results on 4th line on the template with the query "tristesse", the cityblock distance and no clustering
* `python3 result_reader.py joie 4 kmeans` will show you all the results on 4th line on the template with the query "joie" and kmeans clustering
* `python3 result_reader.py joie Results/paper_results.txt minkowski` will show you all the results on the template "paper" with the query "joie" and minkwoski's distance 

Output example :  
```
python3 result_reader.py joie Results/paper_results.txt minkowski
───────-----───────------───────---
 Template line number  0
 POS-TAG Index :
	('*AQ0FS00/morale/moral', 5)
	('*NCFS000/responsabilité/responsabilité', 7)
	('*NCMS000/littérature/littérature', 9)
 Query:joie
 Clustering:knn
 Distance:minkowski
 Result ───────▶ Il n' y a ni joyeuse ni tendresse en inondé .
───────-----───────------───────---
 Template line number  0
 POS-TAG Index :
	('*AQ0FS00/morale/moral', 5)
	('*NCFS000/responsabilité/responsabilité', 7)
	('*NCMS000/littérature/littérature', 9)
 Query:joie
 Clustering:kmeans
 Distance:minkowski
 Result ───────▶ Il n' y a ni vive ni surprise en ravissement .
───────-----───────------───────---
 Template line number  0
 POS-TAG Index :
	('*AQ0FS00/morale/moral', 5)
	('*NCFS000/responsabilité/responsabilité', 7)
	('*NCMS000/littérature/littérature', 9)
 Query:joie
 Clustering:none
 Distance:minkowski
 Result ───────▶ Il n' y a ni joyeuse ni joie en ravissement .
───────-----───────------───────---
From Result file : Results/paper_results.txt
```

## Results Analyzer & filter script

**`result_analyzer.py`** generate barplot of fréquency of words generated.  
It works as a filter as the `result_reader.py` script ; you can filter the element in your histogram according to the given params
**Charts are saved in Results/charts/**

* `python3 result_analyzer.py joie mahalanobis kmeans` will save you an histogram of word generated in eval_results with the query "joie", kmeans clustering and mahalanobis distance. 

![Barplot example](https://gitlab.com/Raymondaud.Q/energie_textuelle/-/raw/main/Results/charts/None_joie_kmeans_mahalanobis_eval_results.png "Bar plot of example command")